import functools
from abc import ABC, abstractmethod

import aiohttp
from markdown import markdown
from nio import AsyncClient, RoomMessageText, InviteEvent


md = functools.partial(markdown, output="html", extensions=["fenced_code"])


class Bot(ABC):

    def __init__(self, client: AsyncClient, passwd: str):
        self.client = client
        self.passwd = passwd

    async def run(self):
        resp = await self.client.login(self.passwd)
        self.user_id = resp.user_id
        resp = await self.client.get_displayname()
        self.display_name = resp.displayname

        resp = await self.client.sync()
        for room_id, _ in resp.rooms.invite.items():
            await self.client.join(room_id)

        self.client.add_event_callback(self.message_cb, RoomMessageText)
        self.client.add_event_callback(self.invite_cb, InviteEvent)
        await self.client.sync_forever(timeout=30000)

    async def close(self):
        self.client.logout()
        self.client.close()

    def send_text(self, room_id, text):
        return self.client.room_send(
            room_id,
            message_type="m.room.message",
            content={
                "msgtype": "m.text",
                "body": text,
                "format": "org.matrix.custom.html",
                "formatted_body": md(text),
            }
        )

    def send_reaction(self, room_id, event_id, key):
        return self.client.room_send(
            room_id,
            message_type="m.reaction",
            content={
                "m.relates_to": {
                    "rel_type": "m.annotation",
                    "event_id": event_id,
                    "key": key,
                }
            }
        )

    def send_error(self, room_id, text):
        return self.send_text(room_id, f"**error**: {text}")

    async def invite_cb(self, room, event):
        await self.client.join(room.room_id)

    @abstractmethod
    async def message_cb(self, room, event):
        pass


async def discover_homeserver(user):
    if ":" not in user:
        return ""
    _, domain = user.split(":")
    async with aiohttp.request('GET', f'https://{domain}/.well-known/matrix/client') as resp:
        resp.raise_for_status()
        if resp.status != 200:
            return ""
        data = await resp.json()
    hs_key = "m.homeserver"
    if hs_key in data and "base_url" in data[hs_key]:
        return data[hs_key]["base_url"]
    return ""
