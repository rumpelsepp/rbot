#!/usr/bin/python3

import argparse
import asyncio
from datetime import datetime, timedelta
import functools
import re
import shlex

from markdown import markdown
from nio import AsyncClient

from bot import Bot, discover_homeserver


md = functools.partial(markdown, output="html", extensions=["fenced_code"])


class RemindBot(Bot):

    def __init__(self, *args, **kwargs):
        self.tasktable = {}
        self.mutex = asyncio.Lock()
        super().__init__(*args, **kwargs)

    async def notify(self, room_id, dt, msg):
        k = dt.isoformat()
        await asyncio.sleep(dt.timestamp() - datetime.now().timestamp())
        # Task got deleted
        async with self.mutex:
            if room_id not in self.tasktable \
              or k not in self.tasktable[room_id]:
                return
            await self.send_text(room_id, msg)
            del self.tasktable[room_id][k]

    def cmd_help(self, room_id):
        text = """```
remindbot usage:

    notify TIMESPEC [MSG] Show MSG (or a generic text if omitted) at TIMESPEC.
    help                  Show this message
```
"""
        return self.send_text(room_id, text)

    async def cmd_table(self, room_id):
        async with self.mutex:
            if room_id not in self.tasktable:
                self.send_text(room_id, "no reminders here.\n")
                return
            table = self.tasktable[room_id]
        res = []
        res.append("running tasks:\n")
        for id_ in table.keys():
            res.append(f"* `{id_}`: {table[id_][1]}")
        await self.send_text(room_id, "\n".join(res))

    async def cmd_rm(self, room_id, args):
        args = shlex.split(args)
        async with self.mutex:
            if room_id not in self.tasktable:
                return
            for id_ in args[1:]:
                if id_ in self.tasktable[room_id]:
                    self.tasktable[room_id][id_][0].cancel()
                    del self.tasktable[room_id][id_]
                    await self.send_text(room_id, f"Ok")
                else:
                    await self.send_error(room_id, f"task {id_} not avail")

    async def cmd_notify(self, room_id, event):
        t = event.body
        prefix = f"{self.display_name}: "
        if not t.startswith(prefix):
            return
        t = t[len(prefix):]

        args = shlex.split(t)
        if len(args) != 2 and len(args) != 3:
            await self.send_error(room_id, "invalid arguments")
            return

        at = parse_timespec(args[1])
        if at is None:
            await self.send_error(room_id, "invalid timespec")
            return

        if len(args) == 3:
            msg = args[2]
        else:
            msg = "Bzzz, bzzz…"

        formatted = at.strftime('%Y-%m-%d %H:%M')
        print(f"Scheduling notification at: {formatted}")
        # Hmmm. Nice idea, but in riot the reaction box is too small
        # for the notification time as a whole…
        await self.send_reaction(room_id, event.event_id, f"👍")
        t = asyncio.create_task(self.notify(room_id, at, msg))
        async with self.mutex:
            if room_id not in self.tasktable:
                self.tasktable[room_id] = {}
            self.tasktable[room_id][at.isoformat()] = (t, msg)

    async def invite_cb(self, room, event):
        await self.client.join(room.room_id)

    async def message_cb(self, room, event):
        if event.sender == self.user_id:
            return

        t = event.body
        prefix = f"{self.display_name}: "
        if not t.startswith(prefix):
            return
        t = t[len(prefix):]

        if t == "help":
            await self.cmd_help(room.room_id)
        elif t == "table":
            await self.cmd_table(room.room_id)
        elif t.startswith("notify"):
            await self.cmd_notify(room.room_id, event)
        elif t.startswith("rm"):
            await self.cmd_rm(room.room_id, t)
        else:
            await self.send_error(room.room_id, f"unrecognized command `{t}`, try `help`")


def parse_timespec(tspec):
    res = None
    try:
        i = int(tspec)
        return datetime.now() + timedelta(seconds=i)
    except ValueError:
        pass

    if (m := re.match(r'\d{4}-\d{2}-\d{2} \d{2}:\d{2}', tspec)):
        res = datetime.strptime(m.group(), "%Y-%m-%d %H:%M")
    elif (m := re.match(r'(?P<key>(tomorrow))(?P<time>\d{2}:\d{2})', tspec)):
        # TODO: Extend to: tomorrow, in 2 days 12:00…
        t = datetime.today()
        d = datetime.strptime(m.group('time'), "%H:%M")
        res = datetime(t.year, t.month, t.day, d.hour, d.minute)
        res += timedelta(days=1)
    elif (m := re.match(r'\d{2}:\d{2}', tspec)):
        t = datetime.today()
        d = datetime.strptime(m.group(), "%H:%M")
        res = datetime(t.year, t.month, t.day, d.hour, d.minute)
    elif (m := re.search(r'([0-9]+[smhd]{0,1})', tspec)):
        now = datetime.now()
        t = m.group()
        quant = t[-1]
        if quant == "s":
            res = now + timedelta(seconds=int(t[:-1]))
        elif quant == "m":
            res = now + timedelta(minutes=int(t[:-1]))
        elif quant == "h":
            res = now + timedelta(hours=int(t[:-1]))
        elif quant == "d":
            res = now + timedelta(days=int(t[:-1]))
        else:
            res = now + timedelta(seconds=int(t))
    return res


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("-u", "--user", help="Specify user to login")
    parser.add_argument("-s", "--homeserver", help="Use this matrix homeserver")
    parser.add_argument("-p", "--passwd", help="User password, be careful!")
    return parser.parse_args()


async def main():
    args = parse_args()
    homeserver = await discover_homeserver(args.user)
    if homeserver == "":
        homeserver = args.homeserver
    client = AsyncClient(homeserver, args.user)
    bot = RemindBot(client, args.passwd)
    try:
        await bot.run()
    except KeyboardInterrupt:
        await bot.close()


if __name__ == "__main__":
    asyncio.run(main())
