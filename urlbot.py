#!/usr/bin/python3

import argparse
import asyncio
import random
import re
import http

import aiohttp
from bs4 import BeautifulSoup
from nio import AsyncClient

from bot import Bot, discover_homeserver

regex = re.compile(r'^(?:(?:https?|ftp)://)(?:\S+(?::\S*)?@)?(?:(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]+-?)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]+-?)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})))(?::\d{2,5})?(?:/[^\s]*)?$')


class URLBot(Bot):

    def __init__(self, *args, quotes=None, gpt2_token=None, **kwargs):
        super().__init__(*args, **kwargs)
        self.quotes = quotes
        self.gpt2_token = gpt2_token

    async def chuck_norris_egg(self, room, event):
        async with aiohttp.request('GET', "https://api.icndb.com/jokes/random") as resp:
            if resp.status != http.HTTPStatus.OK:
                await self.send_text(room.room_id, "chuck norris is not HTTP_OK right now…")
                return
            data = await resp.json()
            if "value" in data and "joke" in data["value"]:
                await self.send_text(room.room_id, data["value"]["joke"])
            else:
                await self.send_text(room.room_id, "chuck norris produces invalid json")

    async def quotes_egg(self, room, event):
        if self.quotes is None:
            await self.send_text(room.room_id, "no")
            return
        q = random.choice(self.quotes)
        await self.send_text(room.room_id, q)

    async def _language_query(self, url, room, event, body):
        if self.gpt2_token is None:
            await self.send_text(room.room_id, "sorry, no license for skynet")
        headers = {"Authorization": f"Bearer {self.gpt2_token}"}
        async with aiohttp.request('POST', url, headers=headers, json=body) as resp:
            if resp.status != http.HTTPStatus.OK:
                await self.send_text(room.room_id, "skynet is afk")
                return
            return await resp.json()

    async def gpt2_egg(self, room, event, body):
        url = "https://api-inference.huggingface.co/models/gpt2-xl"
        data = await self._language_query(url, room, event, body)
        if isinstance(data, list) and "generated_text" in (d := random.choice(data)):
            await self.send_text(room.room_id, d["generated_text"])
        else:
            await self.send_text(room.room_id, "skynet is broken")

    async def bert_egg(self, room, event, body):
        url = "https://api-inference.huggingface.co/models/distilbert-base-uncased"
        data = await self._language_query(url, room, event, body)
        if not isinstance(data, list):
            await self.send_text(room.room_id, "BERT is broken")
            return
        text = ""
        for token in data:
            if "token_str" in token and "score" in token:
                text += f"* **{token['token_str']}**: {token['score']:.4f}\n"
            else:
                await self.send_text(room.room_id, "lol")
        if text != "":
            await self.send_text(room.room_id, text)

    async def process_urls(self, room, event):
        titles = []
        urls = []
        lines = event.body.splitlines()
        for line in lines:
            if m := regex.match(line.strip()):
                urls.append(m.group())

        urls = [get_html_title(url) for url in urls]
        titles = await asyncio.gather(*urls)
        if not titles:
            return

        answer = ""
        if len(titles) == 1:
            answer = f"{titles[0]}"
        else:
            i = 1
            for title in titles:
                if title:
                    answer += f"{i}. {title}\n"
                    i += 1
        await self.send_text(room.room_id, answer)


    async def message_cb(self, room, event):
        if event.sender == self.user_id:
            return

        t = event.body
        prefix = f"{self.display_name}: "
        if t.startswith(prefix):
            t = t[len(prefix):].strip()
            if "chucknorris" in t:
                await self.chuck_norris_egg(room, event)
            elif "quote" in t:
                await self.quotes_egg(room, event)
            elif t.startswith("gpt2"):
                t = t[len("gpt2"):].strip()
                await self.gpt2_egg(room, event, t)
            elif t.startswith("bert"):
                t = t[len("bert"):].strip()
                await self.bert_egg(room, event, t)
            else:
                await self.send_text(room.room_id, "lol")
        else:
            await self.process_urls(room, event)


async def get_html_title(url):
    try:
        async with aiohttp.request('GET', url) as resp:
            if resp.status != http.HTTPStatus.OK:
                return ""
            try:
                soup = BeautifulSoup(await resp.read(), 'html.parser')
                return soup.title.text
            except AttributeError:
                text = soup.get_text()
                text = f"{text[:60]}…" if len(text) > 60 else text
                text = text.strip()
                if text == "":
                    return "no title nor text, bro"
                return text
    except Exception as e:
        return str(e)


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("-u", "--user", help="Specify user to login")
    parser.add_argument("-s", "--homeserver", help="Use this matrix homeserver")
    parser.add_argument("-p", "--passwd", help="User password, be careful!")
    parser.add_argument("-q", "--quotes", help="Quotes file, line separated")
    parser.add_argument("-t", "--gpt2-token", help="API token for GPT2 web api")
    return parser.parse_args()


async def main():
    args = parse_args()
    quotes = None
    if args.quotes:
        with open(args.quotes) as f:
            quotes = f.read().splitlines()
    homeserver = await discover_homeserver(args.user)
    if homeserver == "":
        homeserver = args.homeserver
    client = AsyncClient(homeserver, args.user)
    bot = URLBot(client, args.passwd, quotes=quotes, gpt2_token=args.gpt2_token)
    try:
        await bot.run()
    except KeyboardInterrupt:
        await bot.close()


if __name__ == "__main__":
    asyncio.run(main())

